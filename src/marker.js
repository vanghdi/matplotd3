/**
 * Created by dirkvangheel on 14/10/13.
 *
 * Set of SVG Markers
 */

plotd3.Marker = {

    circleMarker : function (vis, color) {
        var cssId = 'circleMarker-'+color.replace("#","");
        vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "circle-marker")
                .attr("stroke",color)
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("svg:circle")
                .attr("cx",6)
                .attr("cy",6)
                .attr("r",3);
        return cssId;
    },

    pointMarker : function (vis, color) {
        var cssId = 'pointMarker-'+color.replace("#","");
        vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "point-marker")
            .attr("stroke",color)
            .attr("fill", color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
            .append("svg:circle")
            .attr("cx",6)
            .attr("cy",6)
            .attr("r",2);
        return cssId;
    },

    plusMarker : function (vis, color) {
        var cssId = 'plusMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "plus-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 6)
            .attr("x2", 10)
            .attr("y2", 6)
        marker.append("svg:line")
            .attr("x1", 6)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 10);
        return cssId;
    },

    xMarker : function (vis, color) {
        var cssId = 'xMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "x-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 2)
            .attr("x2", 10)
            .attr("y2", 10)
        marker.append("svg:line")
            .attr("x1", 10)
            .attr("y1", 2)
            .attr("x2", 2)
            .attr("y2", 10);
        return cssId;
    },

    oneMarker : function (vis, color) {
        var cssId = 'oneMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "one-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 6)
        marker.append("svg:line")
            .attr("x1", 10)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 6);
        marker.append("svg:line")
            .attr("x1", 6)
            .attr("y1", 10)
            .attr("x2", 6)
            .attr("y2", 6);
        return cssId;
    },

    twoMarker : function (vis, color) {
        var cssId = 'twoMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "two-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 6)
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 10)
            .attr("x2", 6)
            .attr("y2", 6);
        marker.append("svg:line")
            .attr("x1", 10)
            .attr("y1", 6)
            .attr("x2", 6)
            .attr("y2", 6);
        return cssId;
    },

    threeMarker : function (vis, color) {
        var cssId = 'threeMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "three-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 6)
            .attr("x2", 6)
            .attr("y2", 6)
        marker.append("svg:line")
            .attr("x1", 10)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 6);
        marker.append("svg:line")
            .attr("x1", 10)
            .attr("y1", 10)
            .attr("x2", 6)
            .attr("y2", 6);
        return cssId;
    },

    fourMarker : function (vis, color) {
        var cssId = 'fourMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "four-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 10)
            .attr("x2", 6)
            .attr("y2", 6)
        marker.append("svg:line")
            .attr("x1", 10)
            .attr("y1", 10)
            .attr("x2", 6)
            .attr("y2", 6);
        marker.append("svg:line")
            .attr("x1", 6)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 6);
        return cssId;
    },

    verticalLineMarker : function (vis, color) {
        var cssId = 'verticalLineMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "vertical-line-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 6)
            .attr("y1", 2)
            .attr("x2", 6)
            .attr("y2", 10);
        return cssId;
    },

    horizontalLineMarker : function (vis, color) {
        var cssId = 'horizontalLineMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "horizontal-line-marker")
            .attr("stroke",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
        marker.append("svg:line")
            .attr("x1", 2)
            .attr("y1", 6)
            .attr("x2", 10)
            .attr("y2", 6);
        return cssId;
    },

    diamondMarker : function (vis, color) {
        var cssId = 'diamondMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "diamond-marker")
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("path")
                .attr("d", "M6,2 L10,6 L6,10 L2,6 Z")
                .attr("stroke",color)
                .attr("fill",color)
        return cssId;
    },

    thinDiamondMarker : function (vis, color) {
        var cssId = 'thinDiamondMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "thin-diamond-marker")
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("path")
                .attr("d", "M6,2 L8,6 L6,10 L4,6 Z")
                .attr("stroke",color)
                .attr("fill",color)
        return cssId;
    },

    triangleDownMarker : function (vis, color) {
        var cssId = 'triangleDownMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "triangle-down-marker")
                .attr("stroke",color)
                .attr("fill",color)
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("path")
                .attr("d", "M2,2 L10,2 L6,10 Z")
        return cssId;
    },

    triangleUpMarker : function (vis, color) {
        var cssId = 'triangleUpMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
            .attr("id",cssId)
            .attr("class", "triangle-up-marker")
            .attr("stroke",color)
            .attr("fill",color)
            .attr("refX", 6)
            .attr("refY", 6)
            .attr("markerWidth", 12)
            .attr("markerHeight", 12)
            .attr("orient", "0")
            .append("path")
            .attr("d", "M6,2 L10,10 L2,10 Z")
        return cssId;
    },

    triangleRightMarker : function (vis, color) {
        var cssId = 'triangleRightMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "triangle-right-marker")
                .attr("stroke",color)
                .attr("fill",color)
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("path")
                .attr("d", "M2,2 L10,6 L2,10 Z")
        return cssId;
    },

    triangleLeftMarker : function (vis, color) {
        var cssId = 'triangleLeftMarker-'+color.replace("#","");
        var marker = vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "triangle-left-marker")
                .attr("stroke",color)
                .attr("fill",color)
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("path")
                .attr("d", "M2,6 L10,2 L10,10 Z")
        return cssId;
    },

    squareMarker : function (vis, color) {
        var cssId = 'squareMarker-'+color.replace("#","");
        vis.selectAll("#"+cssId)
            .data([1]).enter()
            .append("svg:marker")
                .attr("id",cssId)
                .attr("class", "square-marker")
                .attr("stroke",color)
                .attr("refX", 6)
                .attr("refY", 6)
                .attr("markerWidth", 12)
                .attr("markerHeight", 12)
                .attr("orient", "0")
            .append("svg:rect")
                .attr("x",3)
                .attr("y",3)
                .attr("width",6)
                .attr("height",6);
        return cssId;
    }

};
