/* matplotd3 main */


// Base object.
var plotd3 = function() {
  if (!(this instanceof plotd3)) {
    return new plotd3();
  }
};


// Version.

plotd3.VERSION = '0.0.0';

// Export to the root, which is probably `window`.
root.plotd3 = plotd3;
