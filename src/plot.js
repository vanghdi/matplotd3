/**
 * Created by dirkvangheel on 14/10/13.
 */

plotd3.Plot = Class.extend({

    _cssId:null,
    _plotStats:null,
    _seriesData : null,
    _seriesMeta : null,
    _matplotlibStyleColors : {b: 'blue',g: 'green',r: 'red',c: 'cyan',m: 'magenta',y: 'yellow',k: 'black',w: 'white'},
    _matplotlibStyleLineStyles: {'--':'dashed-line-style','-.':'dash-dot-line-style','-':'solid-line-style',':':'dotted-line-style'},
    _matplotlibStyleMarker : { 'o': 'circleMarker', 's': 'squareMarker', '.': 'pointMarker','+': 'plusMarker','x': 'xMarker',
        'v':'triangleDownMarker','^':'triangleUpMarker','>':'triangleRightMarker','<':'triangleLeftMarker',
        '1':'oneMarker', '2':'twoMarker','3':'threeMarker','4':'fourMarker',
        'D': 'diamondMarker', 'd': 'thinDiamondMarker',
        '_': 'horizontalLineMarker', '|': 'verticalLineMarker'},

    /**
     * Constructor
     * @param cssId {String} css selector of element where graph should be rendered
     */
    init: function(cssId) {
        this._cssId = (cssId.indexOf("#")===0) ? cssId : "#"+cssId;
        this._seriesData = [];
        this._seriesMeta = [];
        this._plotStats = {minXValue:undefined, maxXValue:undefined,
                           minYValue:undefined, maxYValue:undefined,
                           nTicksX: 10, nTicksY:5,
                           paddingX: 15,paddingY: 15
        };
        this._colorScale = d3.scale.category10();
    },

    /**
     * converts x and y arrays into single array with {'x': xValue, 'y':yValue} dictionaries
     * @param x
     * @param y
     * @returns {Array of dictionaries {'x': <value from x>, 'y': <value from y> }
     * @private
     */
    _dictZip: function(x, y){
        return x.map(function(_,i){
            return {'x': x[i], 'y':y[i]};
        })
    },

    /**
     * copies x array into new object
     * @param x
     * @private
     */
    _copyArray: function(x){
        var y = [];
        x.forEach(function (e){
            y.push(e);
        });
        return y;
    },

    /**
     * update min/max X and Y values
     * @param x
     * @param y
     * @private
     */
    _updatePlotStats: function(x,y) {
        var maxXValue = Math.max.apply( Math, x );
        var minXValue = Math.min.apply( Math, x );
        var maxYValue = Math.max.apply( Math, y );
        var minYValue = Math.min.apply( Math, y );
        if (this._plotStats.maxXValue===undefined || this._plotStats.maxXValue < maxXValue ){
            this._plotStats.maxXValue = maxXValue;
        }
        if (this._plotStats.minXValue===undefined || this._plotStats.minXValue > minXValue ){
            this._plotStats.minXValue = minXValue;
        }
        if (this._plotStats.maxYValue===undefined || this._plotStats.maxYValue < maxYValue ){
            this._plotStats.maxYValue = maxYValue;
        }
        if (this._plotStats.minYValue===undefined || this._plotStats.minYValue > minYValue ){
            this._plotStats.minYValue = minYValue;
        }
    },

    _extractStyleElement: function (styleDefinition, style) {
        var regexStr = Object.keys(styleDefinition).join("#")
            .replace(".","\\.")
            .replace("+","\\+")
            .replace("^","\\^")
            .replace("|","\\|")
            .replace("_","\\_")
            .replace(/#/g, "|");
        var rx = new RegExp('('+regexStr+')');
        var arr = rx.exec(style);
        return (arr!==null) ? arr[1] : null;
    },

    _mapStyleInputParameterToMetaAttributes: function (style, sMeta) {
        var color = this._extractStyleElement(this._matplotlibStyleColors, style);
        var lineStyle = this._extractStyleElement(this._matplotlibStyleLineStyles, style)
        var lineMarker = this._extractStyleElement(this._matplotlibStyleMarker, style)
        if (color != null) sMeta.color = this._matplotlibStyleColors[color];
        if (lineStyle != null) sMeta.lineStyleClass = this._matplotlibStyleLineStyles[lineStyle];
        if (lineMarker != null) sMeta.lineMarker = this._matplotlibStyleMarker[lineMarker];
    },

    _mapInputParameterToMetaAttribute: function (params, paramName, sMeta, metaName, defaultValue) {
        if (params !== undefined && paramName in params) {
            sMeta[metaName] = params[paramName];
        } else if (defaultValue!==undefined) {
            sMeta[metaName] = defaultValue;
        }
    },

    plot: function(x, y, style) {
        // TODO: if x or y contains strings, apply ordinal scale
        var s = this._dictZip(x, y);
        var sMeta = { type: 'plot'};
        this._seriesData.push(s);
        this._seriesMeta.push(sMeta);
        this._updatePlotStats(x,y);
        this._mapStyleInputParameterToMetaAttributes(style, sMeta);
    },

    fill_between: function(x, y1, y2, params) {
        // draw (x,y1), then reverse of (x,y2)
        var X = x.concat(this._copyArray(x).reverse());
        var Y = y1.concat(y2.reverse());
        // close path by connecting to first point of (x,y1)
        X.push(x[0]);
        Y.push(y1[0]);
        var sMeta = { type: 'fill'};
        this._mapInputParameterToMetaAttribute(params, 'edgecolor', sMeta, 'color', this._colorScale(this._seriesData.length));
        this._mapInputParameterToMetaAttribute(params, 'facecolor', sMeta, 'fill', this._colorScale(this._seriesData.length));
        var s = this._dictZip(X, Y);
        this._seriesData.push(s);
        this._seriesMeta.push(sMeta);
        this._updatePlotStats(X,Y);
    },

    _setAxis: function (vis, x, y, h, w) {
        var rules = vis.selectAll("g.rule")
            .data(x.ticks(this._plotStats.nTicksX))
            .enter()
            .append("svg:g")
            .attr("class", "rule");

        // Draw grid lines
        rules.append("svg:line")
            .attr("x1", x)
            .attr("x2", x)
            .attr("y1", 0)
            .attr("y2", h - 1)
            .attr("class", "horizontal-ruler");

        rules.append("svg:line")
            .attr("class", "vertical-ruler")
            .data(y.ticks(this._plotStats.nTicksY))
            .attr("y1", y)
            .attr("y2", y)
            .attr("x1", 0)
            .attr("x2", w);

        // Place axis tick labels
        rules.append("svg:text")
            //.data(x.ticks(5))
            .attr("x", x)
            .attr("y", h + 15)
            .attr("dy", ".71em")
            .attr("text-anchor", "middle")
            .attr("class","x-axis-ticks")
            .text(x.tickFormat(10))
            .text(String);

        rules.append("svg:text")
            //.data(y.ticks(10))
            .attr("y", y)
            .attr("x", -10)
            .attr("dy", ".35em")
            .attr("text-anchor", "end")
            .attr("class","y-axis-ticks")
            .text(y.tickFormat(5));
    },

    _isMarkerDefined: function (i) {
        return 'lineMarker' in this._seriesMeta[i];
    },

    _isCustomColorDefined: function (i) {
        return 'color' in this._seriesMeta[i];
    },

    _isCustomFillDefined: function (i) {
        return 'fill' in this._seriesMeta[i];
    },

    _isLineStyleDefined: function (i) {
        return 'lineStyleClass' in this._seriesMeta[i];
    },

    _addPath: function (vis, i, s, x, y) {
        // check styling attributes
        var classes = ['line']
        var color=this._colorScale(i);
        if (this._isCustomColorDefined(i)) {
            color= this._seriesMeta[i]['color'];
        }
        var markerCssId = null;
        if (this._isMarkerDefined(i)) {
            var marker = this._seriesMeta[i].lineMarker;
            markerCssId = plotd3.Marker[marker](vis,color);
        }
        if (this._isLineStyleDefined(i)) {
            classes.push(this._seriesMeta[i].lineStyleClass);
        }

        // create path
        var path = vis.selectAll("#line" + i)
            .data([s])
            .enter()
            .append("svg:path")
            .attr("id", "line" + i)
            .attr("d", d3.svg.line()
                .x(function (d) {
                    return x(d.x);
                })
                .y(function (d) {
                    return y(d.y);
                }))
            .attr("stroke", color)
            .attr('class', classes.join(" "));

        if (this._isCustomFillDefined(i)) {
            path.attr("fill",this._seriesMeta[i].fill);
        } else {
            path.attr("fill","none");
        }
        if (this._isMarkerDefined(i)) {
            path.style("marker-start", "url(#"+markerCssId+")");
            path.style("marker-mid", "url(#"+markerCssId+")");
            path.style("marker-end", "url(#"+markerCssId+")");
            if (!this._isLineStyleDefined(i)) {
                // only display markers, no path
                path.attr("stroke", null);
            }
        }
    },

    show: function () {

        // TODO: retrieve width + height from element
        var  w = 800,
            h = 350,
            p = 30, // padding
            x = d3.scale.linear()
                .domain([ this._plotStats.minXValue, this._plotStats.maxXValue ])
                .range([0, w-this._plotStats.paddingX]),
            y = d3.scale.linear()
                .domain([this._plotStats.minYValue, this._plotStats.maxYValue])
                .range([h-this._plotStats.paddingY, 0]);

        var vis = d3.select(this._cssId)
            .append("svg:svg")
                .attr("width", w + p * 2)
                .attr("height", h + p * 2)
            .append("svg:g")
                .attr("transform", "translate(" + p + "," + p + ")");

        vis.append("svg:rect")
            .attr("class","plot-area")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", w)
            .attr("height", h);

        this._setAxis(vis, x, y, h, w);

        this._seriesData.forEach(function(s, i) {
            this._addPath(vis, i, s, x, y);
        }, this);


       /* // -----------------------------
        // Add Title then Legend
        // -----------------------------
        vis.append("svg:text")
            .attr("x", w/4)
            .attr("y", 20)
            .text("% share of income (excluding capital gains): U.S. 1920-2008");

        vis.append("svg:rect")
            .attr("x", w/2 - 20)
            .attr("y", 50)
            .attr("stroke", "darkblue")
            .attr("height", 2)
            .attr("width", 40);

        vis.append("svg:text")
            .attr("x", 30 + w/2)
            .attr("y", 55)
            .text("Top 5% households");

        vis.append("svg:rect")
            .attr("x", w/2 - 20)
            .attr("y", 80)
            .attr("stroke", "maroon")
            .attr("height", 2)
            .attr("width", 40);

        vis.append("svg:text")
            .attr("x", 30 + w/2)
            .attr("y", 85)
            .text("Top 1% households");

        //});
        */
    }

});
